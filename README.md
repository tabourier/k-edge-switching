### Summary

K-edge switching is an experimental method to generate a sample of random graphs with chosen structural properties, from any graph having these properties (the seed).

It is a Markov chain Monte Carlo method, relying on the iteration of modifications of the structure, starting from the seed to obtain any element of the set.

It can be used in many different graph generation cases, as long as the set of graphs as a given degree sequence. Two codes are available here: 

+ to generate a simple undirected graph, with a prescribed degree sequence and a prescribed number of triangles

+ to generate a simple undirected graph, with a prescribed degree sequence and a prescribed degree correlation sequence

### Content

+ Two source codes (.ml):

	- kedge_tri.ml: to generate a simple undirected graph, with a prescribed degree sequence and a prescribed number of triangles
	- kedge_corr.ml: to generate a simple undirected graph, with a prescribed degree sequence a prescribed degree correlation sequence

+ Corresponding executable codes (Kedge_tri and Kedge_corr), compiled for an Intel x86 architecture

+ two graphs provided in the adjacency edge list format:

	- internet_ap.txt is a model of an access provider network of the Internet, it contains 830 nodes (routers) and 878 edges
	- gp_references.txt is a scientific collaboration network, describing scientists publishing together, it contains 314 nodes (authors) and 262 edges

### About the source code

The source codes in the archive are provided in OCaml, if you are interested in this language, go to https://ocaml.org/

### Author and licence

These programs have been written by Lionel Tabourier.

These programs are free softwares: you can redistribute it and/or modify them under the terms of the Creative Commons 4.0 CC-BY License. 

For more details, see http://creativecommons.org.au/licences.

### Reference

If you use these codes, please cite:

Tabourier, L., Roth, C., & Cointet, J. P. (2011). Generating constrained random graphs using multiple edge switches. Journal of Experimental Algorithmics (JEA), 16, 1-1.
