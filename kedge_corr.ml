(* ocamlopt -o Kedge_corr str.cmxa unix.cmxa nums.cmxa kedge_corr.ml *)

(* ***** author and license ***** *)
(* written by Lionel Tabourier, code under the terms of the Creative Commons 4.0 CC-BY License *)

Random.self_init ();;



(* ***** input definitions ***** *) 

let in_graph = Sys.argv.(1);;
let in_k = Sys.argv.(2);;
let in_num_trials = Sys.argv.(3);;
let in_external_key = Sys.argv.(4);;



(* ***** general tools ***** *)

type couple = {mutable left : int; mutable right : int;};;

let ios x = int_of_string x;;
let foi x = float_of_int x;;
let sof x = string_of_float x;;
let soi x = string_of_int x;;

let add  = Big_int.add_big_int
and sub  = Big_int.sub_big_int
and succ = Big_int.succ_big_int
and pred = Big_int.pred_big_int
and mult = Big_int.mult_big_int
and div  = Big_int.div_big_int
and big_int = Big_int.big_int_of_int
and bi2str = Big_int.string_of_big_int
and str2bi = Big_int.big_int_of_string
and eq = Big_int.eq_big_int
and sqrtbi = Big_int.sqrt_big_int
and ppq = Big_int.le_big_int
and bi2float = Big_int.float_of_big_int;;

let list_ident l1 l2 =
  if (List.length l1 <> List.length l2) then false
  else
    begin
      let lst1 = List.sort compare l1 in
      let lst2 = List.sort compare l2 in
	if (lst1 = lst2) then true else false
    end;;

let rec take_away a = function
  | [] -> []
  | t::q -> if t = a then q else t::(take_away a q);;

let rec contain e = function
  | [] -> false
  | t::q when t = e -> true
  | _::q -> contain e q;;

let rec squeeze list =
  match list with
    | [] -> []
    | a :: tail -> if (contain a tail = false) then a :: (squeeze tail) else squeeze tail;;



(* ***** translating input to Array of list ***** *)

let reader s =
  let r = Str.regexp "[ \t]+" in
  let l = Str.split r s in
  List.map (fun st -> int_of_string st) l;;

let checker s = (s <> "" || s = "");;

let max_link name =
  let data = open_in name in
  let maximum = ref 0 in
    begin
      try
	while (checker (input_line data) = true) do
	  maximum := !maximum +1;
	done;
      with | End_of_file -> close_in data;
    end;
    !maximum;;

let graph_maker name =
  let n = max_link name in
  let graph = Array.make n [] in
  let data = open_in name in
  let counter = ref 0 in
    begin
      try
        while true do
          let line = reader (input_line data) in
            begin
              graph.(!counter) <- line;
              counter := !counter +1;
            end;
        done
      with | End_of_file -> close_in data;
    end;
    graph;;




(* ***** check input format ***** *)

let check_indexation graph =
  let alert = ref false in
  let length = Array.length graph in
    Array.iter (fun x -> List.iter (fun y -> if (y > length) then alert := true) x) graph;
    !alert;;

let check_no_repetition graph = 
  let alert = ref false in
    Array.iter (fun x -> if (List.length (squeeze x) <> List.length x) then alert := true) graph;
    !alert;;

let check_no_self_loop graph =
  let alert = ref false in
    Array.iteri (fun i x -> if (contain i x = true) then begin print_int i; print_string " ";  alert := true; end) graph;
    !alert;;



(* ***** switching on Array of list ***** *)

let edges_counter graph =
  let counter = ref 0 in
    Array.iter (fun x -> counter := !counter + (List.length x)) graph;
    !counter;;

let randomizer input_list =
  let l = ref input_list in
  for i=0 to (List.length input_list) 
  do 
    l := List.fast_sort (fun x y -> if (Random.int 1000 < Random.int 1000) then -1 else 1) !l
  done;
  !l;;

let ael_modified ael mods i =
  try List.assoc i mods
  with Not_found -> ael.(i);;



(* switching loop *)

let switching input num_switches long_switch =

  (* initializing data *)
  let ael_length = Array.length input in
  let el_length = edges_counter input in
  let el = Array.make el_length {left = 0; right = 0} in
  let counter = ref 0 in
    for i=0 to (ael_length -1)
    do
      List.iter (fun x -> el.(!counter) <- {left = i ; right = x}; counter := !counter +1 ) input.(i);
    done;

  let working_graph = Array.make ael_length [] in
  for i=0 to ael_length-1
  do
    working_graph.(i) <- input.(i);
  done;

 (* structures for testing constraints *)
  let degree = Array.make ael_length 0 in
  for i=0 to ael_length-1
  do
    degree.(i) <- List.length working_graph.(i);
  done;

 (* structure particular to undirected graphs *)
  let mirror_edges = Array.make el_length 0 in
    for i=0 to el_length -2
    do
	for j=i+1 to el_length -1 
	do
	  if ( (el.(i)).right = (el.(j)).left && (el.(i)).left = (el.(j)).right )
	  then begin mirror_edges.(i) <- j;  mirror_edges.(j) <- i; end
	done;
    done;

  (* switching process *)
  let switch_trials = ref (big_int 0) in
  while (ppq !switch_trials (pred num_switches))
  do
    begin
      switch_trials := succ !switch_trials;

     (* selecting links randomly *)
      let list_id_edges = ref [] in
      let list_id_forbidden = ref [] in (* specific to undirected graphs *)
	while (List.length !list_id_edges < long_switch)
	do
	  let ran = Random.int (el_length) in
	    if (List.mem ran !list_id_forbidden = false) then 
	      begin 
		list_id_edges := ran :: !list_id_edges;
		list_id_forbidden := ran :: !list_id_forbidden;
		list_id_forbidden := mirror_edges.(ran) :: !list_id_forbidden;
	      end
	done;

      let permutated_list = randomizer !list_id_edges in 
      let table_edge_before = Array.make long_switch {left = 0 ; right = 0 ;} in
      let table_edge_target = Array.make long_switch {left = 0 ; right = 0 ;} in
      let table_id_edge_before = Array.make long_switch 0 in

     (* selecting permutation randomly *)
      let switch_size = ref 0 in
      for i=0 to long_switch -1
      do
	let id_edge_before = List.nth !list_id_edges i in
	let id_edge_target = List.nth permutated_list i in
	if (id_edge_before <> id_edge_target) 
	then 
	  begin
	    table_id_edge_before.(!switch_size) <- id_edge_before;
	    table_edge_before.(!switch_size) <- el.(id_edge_before);
	    table_edge_target.(!switch_size) <- el.(id_edge_target);
	    switch_size := !switch_size + 1;
	  end;
      done;

     (* constraint: no self-loop *)
      let no_self_loop = ref true in
      for j=0 to !switch_size-1
      do
	let departure_before = (table_edge_before.(j)).left in
	let arrival_target = (table_edge_target.(j)).right in
	if ((arrival_target = departure_before) = true) then no_self_loop := false
      done;
 
      let mods = ref [] in
	for j=0 to !switch_size-1
	do
	  begin
	    let departure_before = (table_edge_before.(j)).left in
	    let arrival_before = (table_edge_before.(j)).right in
	    let departure_target = (table_edge_target.(j)).left in
	    let arrival_target = (table_edge_target.(j)).right in

	    let elt_changed_1 =
	      try List.assoc departure_before !mods
	      with Not_found -> working_graph.(departure_before) in
	    if elt_changed_1 <> working_graph.(departure_before) 
	    then
	      mods := (departure_before , arrival_target :: (take_away arrival_before elt_changed_1)) :: 
		(take_away (departure_before , elt_changed_1) !mods) 
	    else
	      mods := (departure_before , arrival_target :: (take_away arrival_before elt_changed_1)) :: !mods; 

	    let elt_changed_2 =
	      try List.assoc arrival_target !mods
	      with Not_found -> working_graph.(arrival_target) in
	    if elt_changed_2 <> working_graph.(arrival_target) 
	    then
	      mods := (arrival_target , departure_before :: (take_away departure_target elt_changed_2)) :: 
		(take_away (arrival_target , elt_changed_2) !mods) 
	    else
	      mods := (arrival_target , departure_before :: (take_away departure_target elt_changed_2)) :: !mods;

	  end
	done;

      (* complex constraint *)
      let complex_constraint = ref true in

      let corr_before = ref [] in
      let corr_after = ref [] in
      for j=0 to !switch_size-1
      do
	begin
	  let departure_before = (table_edge_before.(j)).left in
	  let arrival_before = (table_edge_before.(j)).right in
	  let arrival_target = (table_edge_target.(j)).right in
	    corr_before :=
	      {left = degree.(departure_before) ; right = degree.(arrival_before)} 
	    :: !corr_before;
	    corr_after :=
	      {left = degree.(departure_before) ; right = degree.(arrival_target)} 
	    :: !corr_after;
	end
      done;

      if (list_ident !corr_before !corr_after <> true)
      then complex_constraint := false;

     (* constraint: no repeated link *)
      let no_repetition = ref true in
      for j=0 to !switch_size-1
      do
	let departure_before = (table_edge_before.(j)).left in
	if (List.length (squeeze (ael_modified working_graph !mods departure_before))) 
	  <> List.length (ael_modified working_graph !mods departure_before) 
	then no_repetition := false
      done;


      (* testing and switching *)
      if (!complex_constraint = true && !no_self_loop = true && !no_repetition = true && !switch_size > 0)
      then
	begin

	  (* modifying working graphs and edge-list *)
	  for j=0 to !switch_size-1
	  do
	    begin
	      let id_edge_before = table_id_edge_before.(j) in
	      let id_edge_mirror = mirror_edges.(table_id_edge_before.(j)) in
	      let departure_before = (table_edge_before.(j)).left in
	      let arrival_before = (table_edge_before.(j)).right in
	      let departure_target = (table_edge_target.(j)).left in
	      let arrival_target = (table_edge_target.(j)).right in
	      el.(id_edge_before) <- {left = departure_before ; right = arrival_target};
	      el.(id_edge_mirror) <- {left = arrival_target ; right = departure_before};
	      let temp_list_1 = working_graph.(departure_before) in
	      working_graph.(departure_before) <- arrival_target :: (take_away arrival_before temp_list_1);
	      let temp_list_2 = working_graph.(arrival_target) in
	      working_graph.(arrival_target) <- departure_before :: (take_away departure_target temp_list_2);
	    end
	  done;
	end
    end
  done;
  working_graph;;



(* defining external measure: number of 4-nodes paths *)

let rec selective_neighbors i = function
    [] -> []
  | (a :: l) -> if a > i then a :: (selective_neighbors i l) else selective_neighbors i l;;

let count_4p graph =
  let path_4 = ref 0 in
    Array.iteri (fun i a -> List.iter (fun x -> List.iter 
					 (fun y -> List.iter 
					    (fun z ->  path_4 := !path_4 +1;) (take_away y (take_away x (take_away i graph.(y))))) 
					 (selective_neighbors x (take_away x (take_away i graph.(x))))) 
		   (take_away i a))
      graph;
    foi !path_4;;



(* main execution *)

let make_graph_output ael num_trials k external_key =
  let length = Array.length ael in
  let output_name = "./output_graph_corr_"^k^"_"^external_key in
  let output_channel = open_out output_name in
  for i=0 to length-1
  do
    List.iter (fun x -> 
		 output_string output_channel (string_of_int(x)); 
		 output_string output_channel (" ")) ael.(i);
    output_string output_channel "\n";
  done;
  close_out output_channel;;

let make_measure_output table_avg k external_key =
  let length = Array.length table_avg in
  let output_name = "./output_measure_corr_"^k^"_"^external_key in
  let output_channel = open_out output_name in
    for t=0 to length-1
    do
      output_string output_channel (sof table_avg.(t));
      output_string output_channel "\n";
    done;
    close_out output_channel;;

let main ael_start num_trials k external_key =
  let tab_measure = Array.make 100 0. in
    if (check_indexation ael_start = true 
	|| check_no_repetition ael_start = true
	|| check_no_self_loop ael_start = true)
    then print_string "\nerror in input file format\n"
    else
      begin
	let ael = ref ael_start in
	for t=0 to 99
	do
	  begin

	    let measure = count_4p  !ael in
	    let temp = tab_measure.(t) in 
	    tab_measure.(t) <- temp +. measure;

	    ael := switching !ael (div (str2bi num_trials) (big_int 100)) (ios k);
	  end
	done;
	make_graph_output !ael num_trials k external_key;
	make_measure_output tab_measure k external_key;
      end;;

main (graph_maker in_graph) in_num_trials in_k in_external_key;;
